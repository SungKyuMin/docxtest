const PizZip = require("pizzip");
const Docxtemplater = require("docxtemplater");
const ImageModule  = require("docxtemplater-image-module-free");

const fs = require("fs");
const path = require("path");

module.exports = {
    test : async(req,res,next)=>{

        // img
        let opts = {}
        opts.centered = false; //Set to true to always center images
        opts.fileType = "docx"; //Or pptx

        //Pass your image loader
        opts.getImage = function (tagValue, tagName) {
            //tagValue is 'examples/image.png'
            //tagName is 'image'
            return fs.readFileSync(tagValue);
        }
        opts.getSize = function (img, tagValue, tagName) {
            //img is the image returned by opts.getImage()
            //tagValue is 'examples/image.png'
            //tagName is 'image'
            //tip: you can use node module 'image-size' here
            return [150, 150];
        }

// 
        console.log( path.resolve( "./장비이력서양식_V4.docx"));
        const content = fs.readFileSync(
            path.resolve( "./장비이력서양식_V4.docx"),
            "binary"
        );
        const zip = new PizZip(content);
        const doc = new Docxtemplater(zip, {
            paragraphLoop: true,
            linebreaks: true,
            modules: [new ImageModule(opts)]
        });

        doc.render({
            EQ_NUM: "John",
            Manufacturer_date: "Doe",
            TEAM: "0652455478",
            ScaleRange: "New Website",
            img: "E:/web/testword/test/noname01.png"
        });

        const buf = doc.getZip().generate({
            type: "nodebuffer",
            compression: "DEFLATE",
        });
        fs.writeFileSync(path.resolve("./output.docx"), buf);
    },


    
}



// const imageOpts = {
//     centered: false,
//     getImage: function (tagValue, tagName) {
//         return fs.readFileSync(tagValue);
//     },
//     getSize: function (img, tagValue, tagName) {
//         // it also is possible to return a size in centimeters, like this : return [ "2cm", "3cm" ];
//         return [150, 150];
//     },
// };
